# Docker image for C2 Platform development environment

[![pipeline status](https://gitlab.com/c2platform/docker/c2d/docker/badges/master/pipeline.svg)](https://gitlab.com/c2platform/docker/c2d/docker/-/commits/master) 
[![Latest Release](https://gitlab.com/c2platform/docker/c2d/docker/-/badges/release.svg)](https://gitlab.com/c2platform/docker/c2d/docker/-/releases) 

This project provides a Docker image configuration that extends the base `docker:20.10.16` image by incorporating the C2 Platform development CA Bundle, specifically the `c2.crt` file. The purpose of this Docker image configuration is to enable seamless integration with the C2 Platform by including the necessary CA certificates. By importing the `c2.crt` file, this image ensures secure communication with the C2 Platform's GitLab and Registry components.
