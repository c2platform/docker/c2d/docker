FROM docker:20.10.16

RUN mkdir -p /etc/docker/certs.d/gitlab.c2platform.org/ \
    && mkdir -p /etc/docker/certs.d/registry.c2platform.org/ \
    && cd /etc/docker/certs.d/gitlab.c2platform.org/ \
    && wget -O ca.crt "https://gitlab.com/c2platform/ansible/-/raw/master/.ca/c2/c2.crt" \
    && cd /etc/docker/certs.d/registry.c2platform.org/ \
    && wget -O ca.crt "https://gitlab.com/c2platform/ansible/-/raw/master/.ca/c2/c2.crt"
